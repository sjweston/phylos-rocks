#!/usr/bin/python


import sys, getopt
from functools import reduce


def get_column(index, world):
    '''
    Given a list of lines or varying length,
    return a column list of line[index] or a ' ' from each line
    "normalizing" the list of lines into a NxM matrix
    '''
    col = []

    for line in world:
        try:
            col.append(line[index])
        except IndexError:
            col.append(' ')

    return col

def simulate_gravity(col):
    '''
    Given a column from our world, move rocks left
    until it transforms or can't move given these rules
        1. A '.' is a rock, and falls as far as possible
        2. If a rock is trying to fall to an index (cursor - 1) occupied by a rock,
            it will stack on top of that rock as ':'
        3. If a rock is trying to fall to an index occupied by a ':' stack,
            it stops.
        4. If a rock is trying to fall to an index occupied by a 'T' table,
            it stops.

    Returns transformed column in re-reversed order

    '''

    # reverse column because it's simpler to think
    # about rocks falling to index 0 rather than (last rock) => len(r_col - 1)
    r_col = col[::-1]

    for i,item in enumerate(r_col):
        if item == '.': # a stone
            cursor = i
            move = True
            while cursor > 0 and move:
                # neighbor is a stone => stack to ':'
                if r_col[cursor-1] == '.':
                    r_col[cursor-1] = ':'
                    r_col[cursor] = ' '
                    cursor -= 1
                    move = False
                # neighbor is a table 'T' or a stack of rocks ':' => stop
                elif r_col[cursor-1] == 'T' or r_col[cursor-1] == ':':
                    move = False
                # movable space => move rock into that space
                else:
                    r_col[cursor-1] = '.'
                    r_col[cursor] = ' '
                    cursor -= 1

    # return reverse so we can pretty print top down
    return r_col[::-1]


def pretty_print_world(world):
    for line in world:
        line = [(lambda c : c if c != ' ' else ' ')(c) for c in line]
        print(line)

def pretty_print(col_set):
    longest_col = reduce(lambda x, y: x if len(x) > len(y) else y, col_set)

    for col_index in range(len(longest_col)):
        line = ''
        for col in col_set:
            try:
                line += col[col_index]
            except IndexError:
                line += ' '

        print(line)

def main(argv):
    inputfile = ''

    try:
        opts, args = getopt.getopt(argv, "hi:", ["input="])
    except:
        print("rocks.py -i <inputfile>")
        sys.exit(2)

    if len(opts) is 0:
        print("rocks.py -i <inputfile>")
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print("rocks.py -i <inputfile>")
            sys.exit()
        elif opt in ("-i", "--input"):
            inputfile = arg

    if inputfile == '':
        print("rocks.py -i <inputfile>")
        sys.exit(2)

    with open(inputfile) as f:
        world = f.readlines()

    # strip new lines out
    world = [x.strip('\n') for x in world]

    # the longest line item in our world is the number of columns we'll have
    num_cols = reduce(lambda x, y: x if len(x) > len(y) else y, world)

    cols = []

    for i in range(len(num_cols)):
        col = get_column(i, world)
        cols.append(simulate_gravity(col))

    pretty_print(cols)

if __name__ == '__main__':
    main(sys.argv[1:])
