# README #

This is Sam Weston's implementation of the Fallin' Rocks take home challenge.


### How do I get set up? ###

This application requires a base install of python3; no additional libraries are
required. It is recommended you setup a python3 virtual env to run the application.

### To Execute ###

$: python rocks.py -i <inputfile>

Where inputfile is the initial state of the "world," and output to the console
is the output state of the world after a gravity simulation running according
to the rules outlined in instructions/Fallin' Rocks.pdf

Some test cases can be found in tests/input*.txt files